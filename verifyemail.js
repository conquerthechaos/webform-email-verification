<script>
var jQueryScriptOutputted = false;
var infusionsoft_email_field;
var infusionsoft_webform;
function initJQuery() {
    if (typeof(jQuery) === 'undefined') {
        if (! jQueryScriptOutputted) {
            jQueryScriptOutputted = true;
            document.write("<scr" + "ipt type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></scr" + "ipt>");
        }
        setTimeout("initJQuery()", 50);
    } else {
        jQuery(function() {
            infusionsoft_webform = jQuery("form.infusion-form");
            infusionsoft_email_field =  jQuery("input[name=inf_field_Email]");
            if(infusionsoft_email_field.length > 0){
                infusionsoft_email_field.each(function () {
                    var theField = jQuery(this);
                    var tableParent =  theField.parents('table.infusion-field-container');
                    tableParent
                        .clone()
                        .insertAfter(tableParent)
                        .find('label').removeAttr('for').html('Verify Email *')
                        .parents('table.infusion-field-container')
                        .find('input')
                        .removeAttr('id','inf_field_Email_Verification')
                        .attr('required',true)
                        .attr('name','inf_field_Email_Verification')
                        .addClass('inf_field_Email_Verification')
                        .bind("cut copy paste",function(e) { e.preventDefault(); })
                        .bind("contextmenu",function(e){ e.preventDefault(); }) ;
                });
            }
            infusionsoft_webform.submit(function( event ) {
                var theForm = jQuery(this);
                var Email =  jQuery("input[name=inf_field_Email]", theForm).val();
                var EmailVerification =  jQuery("input[name=inf_field_Email_Verification]", theForm).val();
                jQuery("input[name=inf_field_Email_Verification]", theForm).removeClass('error').removeAttr('style');
                if(Email !== EmailVerification){
                    alert('Your Email Verification did not match!');
                    jQuery("input[name=inf_field_Email_Verification]", theForm).addClass('error').css('border-color','#f34b4b').focus();
                    event.preventDefault();
                }else{
                    return true;
                }
            });
        });
    }
}
initJQuery();
</script>